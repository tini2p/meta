This is the `meta` repository for project management, and other `meta`-related issues.

Please see the [issues](https://gitlab.com/tini2p/meta/issues) for meeting logs, and other related material.